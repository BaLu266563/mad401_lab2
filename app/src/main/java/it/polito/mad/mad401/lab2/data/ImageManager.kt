package it.polito.mad.mad401.lab2.data

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import java.io.File
import java.io.IOException
import java.io.InputStream

class ImageManager() {

    companion object{
        private lateinit var currentPhotoPath: String
        private val requestImageCamera = 1
        private val requestImageGallery = 2
        private val maxSize = 1024


        fun saveImage(ctx : Context, bitmap :Bitmap, path :String?) {
            if(path != null){
                File(path).writeBitmap(bitmap, Bitmap.CompressFormat.PNG, 100)
            }
        }

        private fun File.writeBitmap(bitmap: Bitmap, format: Bitmap.CompressFormat, quality: Int) {
            outputStream().use { out ->
                bitmap.compress(format, quality, out)
                out.flush()
            }
        }

        fun takeFromGallery(fragment :Fragment){
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            fragment.startActivityForResult(intent, requestImageGallery)
        }

        @Throws(IOException::class)
        fun createImageFile(ctx :Context): File {
            // Create an image file name
            val timeStamp = "camera_capture"
            val storageDir: File = ctx.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
            return File.createTempFile(
                "JPEG_${timeStamp}_", /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
            ).apply {
                currentPhotoPath = absolutePath
            }
        }

        fun takeFromCamera(ctx :Context, fragment :Fragment) :String? {
            if (ctx.applicationContext.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
                Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                    takePictureIntent.resolveActivity(ctx.packageManager)?.also {
                        val photoFile: File? = try {
                            createImageFile(ctx)
                        } catch (ex: IOException) {
                            null
                        }
                        photoFile?.also {
                            val photoURI: Uri = FileProvider.getUriForFile(
                                ctx,
                                "it.polito.mad.lab2.fileprovider",
                                it
                            )
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                            fragment.startActivityForResult(takePictureIntent, requestImageCamera)
                            return currentPhotoPath
                        }
                    }
                }

            }else{
                Toast.makeText(ctx, "camera not available", Toast.LENGTH_SHORT).show()
            }
            return null
        }

        fun rotateImage(source: Bitmap, angle: Int): Bitmap {
            val matrix = Matrix()
            matrix.postRotate(angle.toFloat())
            return Bitmap.createBitmap(
                source, 0, 0, source.width, source.height,
                matrix, true
            )
        }

        fun autoRotateImage(ctx :Context, file :File, imgBitmap: Bitmap): Bitmap {
            val imageURI = Uri.fromFile(file)
            val input: InputStream? = ctx.contentResolver.openInputStream(imageURI!!)
            val ei : ExifInterface
            ei = if (Build.VERSION.SDK_INT > 23) {
                ExifInterface(input!!)
            }else {
                ExifInterface(imageURI.path!!)
            }
            return when (ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)) {
                ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(imgBitmap, 90)
                ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(imgBitmap, 180)
                ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(imgBitmap, 270)
                ExifInterface.ORIENTATION_NORMAL -> imgBitmap
                else -> imgBitmap
            }
        }

        fun resizeImageIfNeeded(imgBitmap: Bitmap): Bitmap {
            val imgHeight = imgBitmap.height
            val imgWidth = imgBitmap.width
            val newHeight: Int
            val newWidth: Int
            return if(imgHeight > maxSize || imgWidth > maxSize){
                if(imgHeight > imgWidth){
                    newHeight = maxSize
                    newWidth = (newHeight*imgWidth)/imgHeight
                }else {
                    newWidth = maxSize
                    newHeight = (imgHeight*newWidth)/imgWidth
                }
                Bitmap.createScaledBitmap(imgBitmap, newWidth, newHeight, true)
            }else{
                return imgBitmap
            }
        }
    }
}