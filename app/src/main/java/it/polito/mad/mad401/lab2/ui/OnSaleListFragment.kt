package it.polito.mad.mad401.lab2.ui

import android.os.Bundle
import android.view.*
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import it.polito.mad.mad401.lab2.R
import it.polito.mad.mad401.lab2.data.*
import kotlinx.android.synthetic.main.filter_options_dialog.view.*
import kotlinx.android.synthetic.main.filter_options_dialog.view.category_field
import kotlinx.android.synthetic.main.filter_options_dialog.view.subcategory_field
import kotlinx.android.synthetic.main.onsaleitemlist_fragment.*


private val idMap = mapOf(
    "Arts & Crafts" to R.array.arts,
    "Sports & Hobby" to R.array.sports,
    "Baby" to R.array.baby,
    "Women's fashion" to R.array.women,
    "Men's fashion" to R.array.men,
    "Electronics" to R.array.electronics,
    "Games & Videogames" to R.array.games,
    "Automotive" to R.array.automotive
)

class OnSaleListFragment: Fragment(){

    private var ivm: ItemsViewModel = ItemsViewModel()
    private var uvm: UsersViewModel = UsersViewModel()
    lateinit var adapter :OnSaleItemAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.onsaleitemlist_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivm = ViewModelProvider(requireActivity()).get(ItemsViewModel::class.java)
        uvm = ViewModelProvider(requireActivity()).get(UsersViewModel::class.java)
        ivm.setContext(requireActivity().applicationContext)

        if(uvm.currentUser == null){
            if(findNavController().currentDestination?.id != R.id.signInFragment) {
                findNavController().navigate(R.id.onsale_to_signin)
            }
            return
        }

        ivm.items.observe(viewLifecycleOwner, Observer {
            if (lifecycle.currentState != Lifecycle.State.STARTED) {
                adapter = OnSaleItemAdapter(it, ivm, viewLifecycleOwner)
                recycler_view.adapter = adapter
                swiperefresh.isRefreshing = false
                if (it.isEmpty()) {
                    no_items.visibility = View.VISIBLE
                } else {
                    no_items.visibility = View.GONE
                }
            }
        })

        ivm.getFilteredItems(uvm.me.value!!.id, ivm.filterOptions)

        recycler_view.layoutManager = LinearLayoutManager(requireContext())

        ivm.getItemsImageUrls()

        swiperefresh.setOnRefreshListener {
            ivm.getFilteredItems(uvm.me.value!!.id, ivm.filterOptions)
            ivm.getItemsImageUrls()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.items_onsale_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.menu_filter_options -> {
                val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.filter_options_dialog, null)
                val mBuilder = AlertDialog.Builder(requireContext()).setView(mDialogView).setTitle("Filter Options")
                val mAlertDialog = mBuilder.show()

                mDialogView.search_text.isChecked = ivm.filterOptions.byText
                mDialogView.search_category.isChecked = ivm.filterOptions.byCategory
                mDialogView.search_subcategory.isChecked = ivm.filterOptions.bySubcategory
                mDialogView.search_price_min.isChecked = ivm.filterOptions.byPriceMin
                mDialogView.search_price_max.isChecked = ivm.filterOptions.byPriceMax
                
                mDialogView.text_field.setText(ivm.filterOptions.filter_text)
                mDialogView.category_field.setText(ivm.filterOptions.filter_category)
                mDialogView.subcategory_field.setText(ivm.filterOptions.filter_subcategory)
                mDialogView.price_min_field.setText(ivm.filterOptions.filter_prize_min)
                mDialogView.price_max_field.setText(ivm.filterOptions.filter_prize_max)
                if(ivm.filterOptions.filter_category != ""){
                    val subIndex = idMap[ivm.filterOptions.filter_category]
                    if (subIndex != null) {
                        val array = resources.getStringArray(subIndex)
                        val subadapter =
                            CustomArrayAdapter(
                                requireContext(),
                                R.layout.dropdown_menu_popup_item,
                                array
                            )
                        mDialogView.subcategory_field.setAdapter(subadapter)
                        mDialogView.subcategory_field.isEnabled = true
                    }
                    mDialogView.search_category.isChecked = true
                }
                //impostazione spinner categorie
                val catArray = resources.getStringArray(R.array.categories)
                val adapter = CustomArrayAdapter(requireContext(), R.layout.dropdown_menu_popup_item, catArray)
                mDialogView.category_field.setAdapter(adapter)
                mDialogView.category_field.setOnItemClickListener { parent, _, position, _ ->
                    val subadapter = ArrayAdapter.createFromResource(requireContext(), idMap[parent.getItemAtPosition(position)]!!, R.layout.dropdown_menu_popup_item)
                    mDialogView.subcategory_field.setText("")
                    mDialogView.subcategory_field.setAdapter(subadapter)
                    mDialogView.subcategory_field.isEnabled = true
                    mDialogView.search_category.isChecked = true
                }
                mDialogView.subcategory_field.setOnItemClickListener{ _, _, _, _ ->
                    mDialogView.search_category.isChecked = true
                    mDialogView.search_subcategory.isChecked = true
                }
                mDialogView.text_field.addTextChangedListener {
                    mDialogView.search_text.isChecked = !mDialogView.text_field.text.isNullOrBlank()
                }
                mDialogView.price_min_field.addTextChangedListener {
                    mDialogView.search_price_min.isChecked = !mDialogView.price_min_field.text.isNullOrBlank()
                }
                mDialogView.price_max_field.addTextChangedListener{
                    mDialogView.search_price_max.isChecked = !mDialogView.price_max_field.text.isNullOrBlank()
                }
                mDialogView.search_category.setOnCheckedChangeListener { buttonView, isChecked ->
                    if(!isChecked) mDialogView.search_subcategory.isChecked = false
                }

                mDialogView.filter_ok.setOnClickListener {
                    mAlertDialog.dismiss()

                    if(mDialogView.text_field.text.isNullOrBlank()){
                        mDialogView.search_text.isChecked = false
                    }
                    if(mDialogView.category_field.text.isNullOrBlank()){
                        mDialogView.search_category.isChecked = false
                    }
                    if(mDialogView.subcategory_field.text.isNullOrBlank()){
                        mDialogView.search_subcategory.isChecked = false
                    }
                    if(mDialogView.price_min_field.text.isNullOrBlank()){
                        mDialogView.search_price_min.isChecked = false
                    }
                    if(mDialogView.price_max_field.text.isNullOrBlank()){
                        mDialogView.search_price_max.isChecked = false
                    }

                    ivm.filterOptions.byText = mDialogView.search_text.isChecked
                    ivm.filterOptions.byCategory = mDialogView.search_category.isChecked
                    ivm.filterOptions.bySubcategory = mDialogView.search_subcategory.isChecked
                    ivm.filterOptions.byPriceMin = mDialogView.search_price_min.isChecked
                    ivm.filterOptions.byPriceMax = mDialogView.search_price_max.isChecked

                    ivm.filterOptions.filter_text = mDialogView.text_field.text.toString()
                    ivm.filterOptions.filter_category = mDialogView.category_field.text.toString()
                    ivm.filterOptions.filter_subcategory = mDialogView.subcategory_field.text.toString()
                    ivm.filterOptions.filter_prize_min = mDialogView.price_min_field.text.toString()
                    ivm.filterOptions.filter_prize_max = mDialogView.price_max_field.text.toString()

                    ivm.getFilteredItems(uvm.me.value!!.id, ivm.filterOptions)
                }
                mDialogView.filter_cancel.setOnClickListener {
                    mAlertDialog.dismiss()
                }
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }
}