package it.polito.mad.mad401.lab2.ui

import android.os.Bundle
import android.view.*
import android.widget.ImageView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.navigation.NavigationView
import it.polito.mad.mad401.lab2.R
import it.polito.mad.mad401.lab2.data.ReviewAdapter
import it.polito.mad.mad401.lab2.data.User
import it.polito.mad.mad401.lab2.data.UsersViewModel
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_show_profile.*

class ShowProfileFragment : Fragment(), OnMapReadyCallback {

    private var uvm: UsersViewModel = UsersViewModel()
    private var myself =false
    private val IMAGE_EXTENSION: String = ".PNG"
    private lateinit var mMap :GoogleMap
    private lateinit var adapter: ReviewAdapter
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_show_profile, container, false)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!
        val position : LatLng
        val user :User
        mMap.uiSettings.isCompassEnabled = true
        mMap.uiSettings.isMapToolbarEnabled = false
        mMap.uiSettings.setAllGesturesEnabled(false)
        if(myself){
            user = uvm.me.value?:User()
        }else {
            user = uvm.user.value?:User()
        }
        if(user.lat != null && user.lng != null){
            position = LatLng(user.lat!!, user.lng!!)
            mMap.addMarker(MarkerOptions().position(position).title(user.nickname))
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15F))
            no_position.visibility = View.GONE
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mapview.onCreate(savedInstanceState)
        mapview.onResume()
        uvm = ViewModelProvider(requireActivity()).get(UsersViewModel::class.java)
        val uid: String
        if(arguments != null){
            uid = arguments?.getString("group01.lab2.USERID")!!
        }else{
            uid = uvm.currentUser?.uid!!
        }


        uvm.image_tmp = null

        if(arguments!=null){
            requireActivity().toolbar.title = "User Profile"
            //val uid = arguments?.getString("group01.lab2.USERID")!!
            uvm.getUser(uid)
            nickname_icon.visibility = View.VISIBLE
            uvm.user.observe(viewLifecycleOwner, Observer {
                mapview.getMapAsync(this)
                full_name_wrapper.visibility=View.GONE
                name_icon.visibility=View.GONE
                nickname_wrapper.text = it.nickname
                location_wrapper.text = it.location
                email_address_wrapper.text = it.email
                phone_number_wrapper.text = it.phonenumber
                user_rating.rating = it.rating
                if(it.rating_number == 1){
                    ratings_number.text = "${it.rating_number.toString()} rating"
                }else{
                ratings_number.text = "${it.rating_number.toString()} ratings"
            }
            })
            if(uvm.image == null) {
                uvm.getUserImageUrl(uid + IMAGE_EXTENSION)
                uvm.userImageUrl.observe(viewLifecycleOwner, Observer {
                    if (it != "") {
                        Glide.with(this)
                            .load(it)
                            .placeholder(R.drawable.profile_placeholder)
                            .into(photo_profile)
                    } else {
                        photo_profile.setImageResource(R.drawable.profile_placeholder)
                    }
                })
            }else{
                photo_profile.setImageBitmap(uvm.image)
                requireActivity().findViewById<NavigationView>(R.id.nav_view)
                    .getHeaderView(0)
                    .findViewById<ImageView>(R.id.profile_image_header).setImageBitmap(uvm.image)
            }
        }else {
            requireActivity().toolbar.title = "My Profile"
            nickname_icon.visibility = View.INVISIBLE
            uvm.getMe(uid)
            myself = true
            uvm.me.observe(viewLifecycleOwner, Observer {
                mapview.getMapAsync(this)
                full_name_wrapper.text = it.fullname
                nickname_wrapper.text = it.nickname
                location_wrapper.text = it.location
                email_address_wrapper.text = it.email
                phone_number_wrapper.text = it.phonenumber
                user_rating.rating = it.rating
                if(it.rating_number == 1){
                    ratings_number.text = "${it.rating_number.toString()} rating"
                }else{
                    ratings_number.text = "${it.rating_number.toString()} ratings"
                }
            })
            if(uvm.image == null){
                //uvm.getMyImageUrl(uvm.currentUser?.uid!!+IMAGE_EXTENSION)
                uvm.getMyImageUrl(uid+IMAGE_EXTENSION)
                uvm.myImageUrl.observe(viewLifecycleOwner, Observer{
                    if(it != ""){
                        Glide.with(this)
                            .load(it)
                            .placeholder(R.drawable.profile_placeholder)
                            .into(photo_profile)
                    }else{
                        photo_profile.setImageResource(R.drawable.profile_placeholder)
                    }
                })
            }else{
                photo_profile.setImageBitmap(uvm.image)
                requireActivity().findViewById<NavigationView>(R.id.nav_view)
                    .getHeaderView(0)
                    .findViewById<ImageView>(R.id.profile_image_header).setImageBitmap(uvm.image)
            }
        }
        recyclerview_reviews.layoutManager = LinearLayoutManager(requireContext())
        uvm.getReviews(uid)
        uvm.reviews.observe(viewLifecycleOwner, Observer {
            if (lifecycle.currentState != Lifecycle.State.STARTED) {
                adapter = ReviewAdapter(it)
                recyclerview_reviews.adapter = adapter
            }
        })

        review_click_area.setOnClickListener{
            if(recyclerview_reviews.isVisible){
                recyclerview_reviews.visibility = View.GONE
                reviews_arrow.setImageResource(R.drawable.ic_arrow_right)
            }else{
                recyclerview_reviews.visibility = View.VISIBLE
                reviews_arrow.setImageResource(R.drawable.ic_arrow_down)
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        if(myself){
            inflater.inflate(R.menu.edit_menu, menu)
        }
        return
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.edit -> {
                uvm.image = null
                if(findNavController().currentDestination?.id != R.id.editProfileFragment) {
                    findNavController().navigate(R.id.showprofile_to_editprofile)
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


}