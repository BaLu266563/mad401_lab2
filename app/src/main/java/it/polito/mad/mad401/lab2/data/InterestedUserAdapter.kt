package it.polito.mad.mad401.lab2.data

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.os.bundleOf
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import it.polito.mad.mad401.lab2.R

class InterestedUserAdapter (
    private val users: List<User>,
    private val itemId: String,
    private val uvm: UsersViewModel,
    private val ivm: ItemsViewModel,
    private val viewLifecycleOwner: LifecycleOwner
): RecyclerView.Adapter<InterestedUserAdapter.ViewHolder>() {

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val image: ImageView = v.findViewById(R.id.image_external)
        val nickname: TextView = v.findViewById(R.id.nickname_external)
        val card: CardView = v.findViewById(R.id.card_id)
        val sellButton: Button = v.findViewById(R.id.sell_button)
        private val IMAGE_EXTENSION = ".PNG"

        fun bind(
            u: User,
            position: Int,
            itemId: String,
            uvm: UsersViewModel,
            ivm: ItemsViewModel,
            viewLifecycleOwner: LifecycleOwner
        ) {
            nickname.text = u.nickname
            ivm.item.observe(viewLifecycleOwner, Observer {
                if(it.status == "Sold"){
                    sellButton.visibility = View.GONE
                }
            })
            uvm.getUserImageUrl(u.id+IMAGE_EXTENSION)
            uvm.usersImageUrls.observe(viewLifecycleOwner, Observer {
                if(it.containsKey(u.id)){
                    Glide.with(card.context)
                        .load(it[u.id])
                        .placeholder(R.drawable.profile_placeholder)
                        .into(image)
                }else{
                    image.setImageResource(R.drawable.profile_placeholder)
                }
            })
            card.setOnClickListener {
                val bundle = bundleOf("group01.lab2.USERID" to u.id)
                if(it.findNavController().currentDestination?.id != R.id.nav_showprofile) {
                    it.findNavController().navigate(R.id.interests_to_show, bundle)
                }
            }
            sellButton.setOnClickListener {
                ivm.getInterestedUserIds(ivm.item.value!!, "Sold")
                ivm.sellItem(FocusedItem(itemId, u.id))
                Snackbar.make(card, "${ivm.item.value!!.title} has been sold to ${u.nickname}!", 5000).show()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_user_layout, parent, false) //false per non connetterlo subito

        return ViewHolder(v)
    }

    override fun getItemCount() = users.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(users[position], position, itemId, uvm, ivm, viewLifecycleOwner)
    }
}