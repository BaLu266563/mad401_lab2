package it.polito.mad.mad401.lab2.ui

import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.*
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import it.polito.mad.mad401.lab2.R
import it.polito.mad.mad401.lab2.data.*
import kotlinx.android.synthetic.main.item_details_fragment.*


class ItemDetailsFragment : Fragment(), OnMapReadyCallback {

    private var mine = false
    private var ivm: ItemsViewModel = ItemsViewModel()
    private var uvm: UsersViewModel = UsersViewModel()
    private var itemId :String = ""
    private var interested :Boolean = false
    private val IMAGE_EXTENSION: String = ".PNG"
    private lateinit var mMap : GoogleMap

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.item_details_fragment, container, false)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!
        val position :LatLng
        mMap.uiSettings.isCompassEnabled = true
        mMap.uiSettings.isMapToolbarEnabled = false
        mMap.uiSettings.setAllGesturesEnabled(false)
        if(ivm.item.value?.lat != null && ivm.item.value?.lng != null){
            position = LatLng(ivm.item.value?.lat!!, ivm.item.value?.lng!!)
            mMap.addMarker(MarkerOptions().position(position).title("Item"))
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15F))
            no_position.visibility = View.GONE
        }
        if(!mine) {
            mMap.setOnMapClickListener {
                if(findNavController().currentDestination?.id != R.id.mapFragment) {
                    findNavController().navigate(R.id.details_to_map)
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mapview.onCreate(savedInstanceState)
        mapview.onResume()

        ivm = ViewModelProvider(requireActivity()).get(ItemsViewModel::class.java)
        uvm = ViewModelProvider(requireActivity()).get(UsersViewModel::class.java)
        ivm.image_tmp=null

        itemId = ivm.itemSelected
        mine = ivm.mine

        if(mine){
            fab_interests.setImageResource(R.drawable.ic_list_user_interested)
        }else{
            val focusedItem = FocusedItem(itemId, uvm.currentUser?.uid!!)
            uvm.checkInterested(focusedItem)
            uvm.result.observe(viewLifecycleOwner, Observer {
                if (lifecycle.currentState != Lifecycle.State.STARTED) {
                    if(it != null) {
                        uvm.result.value = null
                        if (it) {
                            fab_interests.setImageResource(R.drawable.ic_favorite)
                            interested = true
                        } else {
                            fab_interests.setImageResource(R.drawable.ic_favorite_border)
                            interested = false
                        }
                    }
                }
            })
        }

        ivm.getRealTimeItem(itemId)
        ivm.item.observe(viewLifecycleOwner, Observer {
            Log.d("observer", ivm.item.value.toString() + lifecycle.currentState.toString())
            if (lifecycle.currentState != Lifecycle.State.STARTED) {
                mapview.getMapAsync(this)
                title_wrapper.text = it.title
                price_wrapper.text = it.price
                description_wrapper.text = it.description
                category.text = it.category
                subcategory.text = it.subcategory
                location_wrapper.text = it.location
                date_wrapper.text = it.date
                status.text = it.status
                if (status.text == "Sold" && ivm.mine) {
                    requireActivity().invalidateOptionsMenu()
                    fab_interests.hide()
                } else {
                    fab_interests.show()
                }
                ivm.item.value!!.id = it.id
                if (status.text == "Available") {
                    circle_status.setColorFilter(requireContext().resources.getColor(R.color.available))
                } else if (status.text == "Not Available") {
                    circle_status.setColorFilter(requireContext().resources.getColor(R.color.not_available))
                } else if (status.text == "Sold") {
                    circle_status.setColorFilter(requireContext().resources.getColor(R.color.sold))
                }
                if (mine) {
                    seller_icon.visibility = View.GONE
                    seller.visibility = View.GONE
                    seller_rating.visibility = View.GONE
                } else {
                    fab_interests.show()
                    uvm.getUser(it.seller)
                }
                ivm.getItemImageUrl(itemId + IMAGE_EXTENSION)
            }
        })
        uvm.user.observe(viewLifecycleOwner, Observer{
            if (lifecycle.currentState != Lifecycle.State.STARTED) {

                seller.text = Html.fromHtml("<u>"+it.nickname+"</u>")
                seller_rating.rating = it.rating
            }
        })
        if(ivm.image == null){
            ivm.getItemImageUrl(itemId+IMAGE_EXTENSION)
            ivm.itemImageUrl.observe(viewLifecycleOwner, Observer {
                if (lifecycle.currentState != Lifecycle.State.STARTED) {
                    if (it != "") {
                        Glide.with(this)
                            .load(it)
                            .placeholder(R.drawable.product_placeholder)
                            .into(product_image)
                    } else {
                        product_image.setImageResource(R.drawable.product_placeholder)
                    }
                }
            })
        }else{
            product_image.setImageBitmap(ivm.image)
            ivm.image = null
        }

        fab_interests.setOnClickListener {
            if(mine){
                ivm.itemSelected = itemId
                if(findNavController().currentDestination?.id != R.id.usersInterestedFragment) {
                    findNavController().navigate(R.id.details_to_interested)
                }
            }else {
                if(interested){
                    val focusedItem = FocusedItem(ivm.item.value?.id!!, uvm.currentUser?.uid!!)
                    ivm.removeInterestedUser(focusedItem)
                    if(ivm.item.value!!.status == "Sold"){
                        fab_interests.visibility = View.GONE
                    }
                }else {
                    val focusedItem = FocusedItem(ivm.item.value?.id!!, uvm.currentUser?.uid!!)
                    ivm.sendNotificationToRemote(ivm.item.value?.seller!!, "New user interested", "Product: ${ivm.item.value?.title}")
                    ivm.addInterest(focusedItem)
                }
                fab_interests.isClickable = false
            }
        }

        if(!mine){
            ivm.result.observe(viewLifecycleOwner, Observer {
                if (lifecycle.currentState != Lifecycle.State.STARTED) {
                    fab_interests.isClickable = true
                    if (it && !mine) {
                        if (interested) {
                            fab_interests.setImageResource(R.drawable.ic_favorite_border)
                            interested = false
                        } else {
                            fab_interests.setImageResource(R.drawable.ic_favorite)
                            interested = true
                        }
                    } else {
                        Snackbar.make(requireView(), "Impossible to contact the database", Snackbar.LENGTH_LONG).show()
                    }
                }
            })
        }
        seller.setOnClickListener {
            val bundle = bundleOf("group01.lab2.USERID" to ivm.item.value?.seller)
            if(findNavController().currentDestination?.id != R.id.nav_showprofile) {
                findNavController().navigate(R.id.details_to_show, bundle)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        ivm.item.observe(viewLifecycleOwner, Observer {
            Log.d("observer", "on create options menu")
            if (lifecycle.currentState != Lifecycle.State.STARTED) {
                menu.clear()
                if (it.status != "Sold" && mine) {
                    inflater.inflate(R.menu.edit_menu, menu)
                }
            }
        })

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.edit -> {
                ivm.latTmp = null
                ivm.lngTmp = null
                if(findNavController().currentDestination?.id != R.id.itemEditFragment) {
                    findNavController().navigate(R.id.details_to_edit)
                }
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onPause() {
        super.onPause()
        ivm.stopRealTimeItem()
    }
}