package it.polito.mad.mad401.lab2.data

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import it.polito.mad.mad401.lab2.R

class SoldItemAdapter (
    private val items: List<Item>,
    private val ivm: ItemsViewModel,
    private val uvm: UsersViewModel,
    private val viewLifecycleOwner: LifecycleOwner
): RecyclerView.Adapter<SoldItemAdapter.ViewHolder>() {

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val image: ImageView = v.findViewById(R.id.image_external)
        val title: TextView = v.findViewById(R.id.title_external)
        val price: TextView = v.findViewById(R.id.price_external)
        val card: CardView = v.findViewById(R.id.card_id)
        val ratingButton: ImageButton = v.findViewById(R.id.rating_button)

        fun bind(
            i: Item,
            position: Int,
            ivm: ItemsViewModel,
            uvm: UsersViewModel,
            viewLifecycleOwner: LifecycleOwner
        ) {
            title.text = i.title
            price.text = i.price

            ivm.itemsImageUrls.observe(viewLifecycleOwner, Observer {
                if (it.containsKey(i.id)) {
                    Glide.with(card.context)
                        .load(it[i.id])
                        .placeholder(R.drawable.product_placeholder)
                        .into(image)
                } else {
                    image.setImageResource(R.drawable.product_placeholder)
                }
            })

            card.setOnClickListener {
                ivm.itemSelected = i.id
                ivm.mine = false
                if(it.findNavController().currentDestination?.id != R.id.itemDetailsFragment) {
                    it.findNavController().navigate(R.id.bought_to_details)
                }
            }

            ratingButton.visibility = View.GONE
            uvm.checkReview(FocusedItem(i.id, uvm.currentUser?.uid!!))
            uvm.focusedItems.observe(viewLifecycleOwner, Observer{
                if(it.containsKey(i.id)){
                    ratingButton.visibility = View.GONE
                }else{
                    ratingButton.visibility = View.VISIBLE
                }

            })

            ratingButton.setOnClickListener{
                ivm.review = FocusedItem(
                    user = uvm.currentUser?.uid!!,
                    item =i.id,
                    seller = i.seller
                )
                it.findNavController().navigate(R.id.boughts_to_review)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_item_bought_layout, parent, false) //false per non connetterlo subito

        return ViewHolder(v)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position], position, ivm, uvm, viewLifecycleOwner)
    }
}