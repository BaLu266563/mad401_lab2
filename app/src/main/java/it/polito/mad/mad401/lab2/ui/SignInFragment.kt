package it.polito.mad.mad401.lab2.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.messaging.FirebaseMessaging
import it.polito.mad.mad401.lab2.R
import it.polito.mad.mad401.lab2.data.UsersViewModel
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.login_page.*


class SignInFragment: Fragment(){

    val RC_SIGN_IN: Int = 1
    lateinit var mGoogleSignInClient: GoogleSignInClient
    lateinit var mGoogleSignInOptions: GoogleSignInOptions
    private lateinit var firebaseAuth: FirebaseAuth
    private var uvm: UsersViewModel = UsersViewModel()
    private val IMAGE_EXTENSION = ".PNG"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                requireActivity().finish()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)

        return inflater.inflate(R.layout.login_page, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().toolbar.isVisible = false
        firebaseAuth = FirebaseAuth.getInstance()
        uvm = ViewModelProvider(requireActivity()).get(UsersViewModel::class.java)
        uvm.me.observe(viewLifecycleOwner, Observer {
            if (lifecycle.currentState != Lifecycle.State.STARTED) {
                if (uvm.currentUser != null) {
                    FirebaseMessaging.getInstance().isAutoInitEnabled = true
                    if (uvm.me.value?.nickname == "") {
                        uvm.me.value?.fullname = uvm.currentUser?.displayName!!
                        uvm.me.value?.email = uvm.currentUser?.email!!
                        uvm.me.value?.phonenumber = uvm.currentUser?.phoneNumber ?: ""
                        if(findNavController().currentDestination?.id != R.id.editProfileFragment) {
                            requireActivity().toolbar.isVisible = true
                            findNavController().navigate(R.id.signin_to_editprofile)
                        }
                    } else {
                        if(findNavController().currentDestination?.id != R.id.nav_onsalelist_items) {
                            requireActivity().toolbar.isVisible = true
                            findNavController().navigate(R.id.signin_to_onsalelist)
                        }
                    }
                }
            }
        })
        configureGoogleSignIn()
        sign_in_button.setOnClickListener {
            signIn()
        }
    }

    override fun onStart() {
        super.onStart()
        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            uvm.currentUser = user
            uvm.getMe(uvm.currentUser!!.uid)
            uvm.getMyImageUrl(uvm.currentUser!!.uid+IMAGE_EXTENSION)
        }
    }

    private fun configureGoogleSignIn() {
        mGoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(requireContext(), mGoogleSignInOptions)
    }

    private fun signIn() {
        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                if (account != null) {
                    firebaseAuthWithGoogle(account)
                }
            } catch (e: ApiException) {
                Toast.makeText(requireContext(), "Google sign-in failed "+ e.toString(), Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener {
            if (it.isSuccessful) {
                uvm.currentUser = firebaseAuth.currentUser!!
                uvm.getMe(uvm.currentUser!!.uid)
                uvm.getMyImageUrl(uvm.currentUser!!.uid+IMAGE_EXTENSION)
            } else {
                Toast.makeText(requireContext(), "Google sign-in failed", Toast.LENGTH_LONG).show()
            }
        }
    }



}