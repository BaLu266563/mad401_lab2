package it.polito.mad.mad401.lab2.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import it.polito.mad.mad401.lab2.R
import it.polito.mad.mad401.lab2.data.ItemAdapter
import it.polito.mad.mad401.lab2.data.ItemsViewModel
import it.polito.mad.mad401.lab2.data.UsersViewModel
import kotlinx.android.synthetic.main.itemlist_fragment.*

class ItemListFragment: Fragment(){

    private var ivm :ItemsViewModel = ItemsViewModel()
    private var uvm :UsersViewModel = UsersViewModel()
    lateinit var adapter :ItemAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.itemlist_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivm = ViewModelProvider(requireActivity()).get(ItemsViewModel::class.java)
        uvm = ViewModelProvider(requireActivity()).get(UsersViewModel::class.java)
        ivm.setContext(requireActivity().applicationContext)
        ivm.image_tmp = null

        ivm.getMyItems(uvm.currentUser?.uid!!)
        ivm.myItems.observe(viewLifecycleOwner, Observer {
            if (lifecycle.currentState != Lifecycle.State.STARTED) {
                adapter = ItemAdapter(it, ivm, viewLifecycleOwner)
                swiperefresh.isRefreshing = false
                recycler_view.adapter = adapter
                if (it.isEmpty()) {
                    no_items.visibility = View.VISIBLE
                } else {
                    no_items.visibility = View.GONE
                }
            }
        })

        ivm.getItemsImageUrls()

        recycler_view.layoutManager = LinearLayoutManager(context)

        swiperefresh.setOnRefreshListener {
            ivm.getMyItems(uvm.currentUser?.uid!!)
            ivm.getItemsImageUrls()
        }

        addItem.setOnClickListener{
            ivm.itemSelected = ""
            ivm.latTmp = null
            ivm.lngTmp = null
            if(it.findNavController().currentDestination?.id != R.id.itemEditFragment) {
                it.findNavController().navigate(R.id.list_to_edit)
            }
        }
    }
}