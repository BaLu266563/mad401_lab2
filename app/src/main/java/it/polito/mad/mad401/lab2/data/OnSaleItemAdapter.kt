package it.polito.mad.mad401.lab2.data

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import it.polito.mad.mad401.lab2.R

class OnSaleItemAdapter(private val items: List<Item>, private val ivm :ItemsViewModel, private val lifecycleOwner: LifecycleOwner): RecyclerView.Adapter<OnSaleItemAdapter.ViewHolder>()  {

    class ViewHolder(v: View):RecyclerView.ViewHolder(v){
        val image: ImageView = v.findViewById(R.id.image_external)
        val title: TextView = v.findViewById(R.id.title_external)
        val location: TextView = v.findViewById(R.id.location_external)
        val price: TextView = v.findViewById(R.id.price_external)
        val card: CardView = v.findViewById(R.id.card_id)
        val button: ImageButton = v.findViewById(R.id.edit_item_button)
        val status_icon: ImageView = v.findViewById(R.id.circle_status_external)

        fun bind(
            i: Item,
            position: Int,
            ivm: ItemsViewModel,
            lifecycleOwner: LifecycleOwner
        ){
            title.text = i.title
            location.text = i.location
            price.text = i.price
            button.visibility = View.GONE
            status_icon.visibility = View.GONE
            ivm.itemsImageUrls.observe(lifecycleOwner, Observer {
                if(it.containsKey(i.id)){
                    Glide.with(button.context)
                        .load(it[i.id])
                        .placeholder(R.drawable.product_placeholder)
                        .into(image)
                }else{
                    image.setImageResource(R.drawable.product_placeholder)
                }
            })
            //comunico alla ItemDetails quale Item ho scelto
            card.setOnClickListener {
                ivm.itemSelected = i.id
                ivm.mine = false
                if(it.findNavController().currentDestination?.id != R.id.itemDetailsFragment) {
                    it.findNavController().navigate(R.id.onsalelist_to_details)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_item_layout, parent, false)

        return ViewHolder(v)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position], position, ivm, lifecycleOwner)
    }


}