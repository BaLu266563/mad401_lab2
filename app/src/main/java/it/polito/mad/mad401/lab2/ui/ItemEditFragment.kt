package it.polito.mad.mad401.lab2.ui


import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.location.Geocoder
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.view.*
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.snackbar.Snackbar
import it.polito.mad.mad401.lab2.R
import it.polito.mad.mad401.lab2.data.*
import kotlinx.android.synthetic.main.item_edit_fragment.*
import kotlinx.android.synthetic.main.map_dialog.view.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class ItemEditFragment: Fragment(), OnMapReadyCallback {

    private var ivm :ItemsViewModel = ItemsViewModel()
    private var uvm :UsersViewModel = UsersViewModel()
    private val requestImageCamera = 1
    private val requestImageGallery = 2
    private var currentPhotoPath: String? = null
    private var rotation = 0
    private var itemId: String = ""
    private var hasErrors = false
    private val IMAGE_EXTENSION =".PNG"
    private lateinit var mMap : GoogleMap

    private val idMap = mapOf(
        "Arts & Crafts" to R.array.arts,
        "Sports & Hobby" to R.array.sports,
        "Baby" to R.array.baby,
        "Women's fashion" to R.array.women,
        "Men's fashion" to R.array.men,
        "Electronics" to R.array.electronics,
        "Games & Videogames" to R.array.games,
        "Automotive" to R.array.automotive
    )

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.item_edit_fragment, container, false)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!
        val position : LatLng
        mMap.uiSettings.isCompassEnabled = true
        mMap.uiSettings.isMapToolbarEnabled = false
        mMap.uiSettings.setAllGesturesEnabled(true)
        val marker = mMap.addMarker(MarkerOptions().position(LatLng(0.0, 0.0)).title("Item").visible(false))

        if(ivm.latTmp != null && ivm.lngTmp != null){
            position = LatLng(ivm.item.value?.lat!!, ivm.item.value?.lng!!)
            marker.position = position
            marker.showInfoWindow()
            marker.isVisible = true
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15F))
        }

        mMap.setOnMapLongClickListener {
            marker.position = it
            marker.isVisible = true
            ivm.item.value?.lat = marker.position.latitude
            ivm.item.value?.lng = marker.position.longitude
            ivm.latTmp = marker.position.latitude
            ivm.lngTmp = marker.position.longitude
            val geocoder = Geocoder(requireContext(), Locale.getDefault())
            try {
                val address =
                    geocoder.getFromLocation(marker.position.latitude, marker.position.longitude, 1)
                if (address.size == 1) {
                    if (address[0].locality != null) {
                        location.setText(address[0].locality.toString())
                    }
                }
            }catch (e :Exception){
                location.setText("")
            }
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivm = ViewModelProvider(requireActivity()).get(ItemsViewModel::class.java)
        uvm = ViewModelProvider(requireActivity()).get(UsersViewModel::class.java)

        itemId = ivm.itemSelected
        if(itemId != "") {
            title.setText(ivm.item.value?.title)
            price.setText(ivm.item.value?.price)
            description.setText(ivm.item.value?.description)
            category.setText(ivm.item.value?.category, false)
            subcategory.setText(ivm.item.value?.subcategory, false)
            location.setText(ivm.item.value?.location)
            date.setText(ivm.item.value?.date)
            status.setText(ivm.item.value?.status, false)
            ivm.latTmp = ivm.item.value?.lat
            ivm.lngTmp = ivm.item.value?.lng
        }
        //quando lo schermo viene ruotato si applica nuovamente la rotazione
        if(savedInstanceState != null && savedInstanceState.containsKey("rotazione")){
            rotation = savedInstanceState.getInt("rotazione")
            if(rotation != 0){
                var bitmap = product_image.drawable.toBitmap()
                bitmap = ImageManager.rotateImage(bitmap, rotation)
                product_image.setImageBitmap(bitmap)
            }
        }

        //gestione rotazione immagine attraverso bottone
        rotateButton.setOnClickListener {
            var bitmap = product_image.drawable.toBitmap()
            bitmap = ImageManager.rotateImage(bitmap, 90)
            product_image.setImageBitmap(bitmap)
            rotation = (rotation + 90)%360
            ivm.image_tmp = bitmap
        }

        if(ivm.image_tmp ==null){
            ivm.getItemImageUrl(itemId+IMAGE_EXTENSION)
            ivm.itemImageUrl.observe(viewLifecycleOwner, Observer {
                if(it != "") {
                    Glide.with(this)
                        .load(it)
                        .placeholder(R.drawable.product_placeholder)
                        .into(product_image)
                }else {
                    product_image.setImageResource(R.drawable.product_placeholder)
                }
            })
        }else{
            product_image.setImageBitmap(ivm.image_tmp)
        }

        if(savedInstanceState !=null){
            hasErrors = savedInstanceState.getBoolean("hasErrors")
            category.setText(savedInstanceState.getString("category"), false)
            subcategory.setText(savedInstanceState.getString("subcategory"), false)
            status.setText(savedInstanceState.getString("status"), false)
            currentPhotoPath = savedInstanceState.getString("currentPath")
        }

        if(savedInstanceState != null || itemId != ""){
            //gestione subcategorie sapendo la categoria del prodotto
            val subIndex = idMap[category.text.toString()]
            if (subIndex != null) {
                val array = resources.getStringArray(subIndex)
                val subadapter =
                    CustomArrayAdapter(
                        requireContext(),
                        R.layout.dropdown_menu_popup_item,
                        array
                    )
                subcategory.setAdapter(subadapter)
                subcategory.isEnabled = true
            }
        }

        if(hasErrors){
            changeErrorStatus()
        }


        map_edit.setOnClickListener {
            val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.map_dialog, null)
            val mBuilder = AlertDialog.Builder(requireContext()).setView(mDialogView)

            mDialogView.map_view.onCreate(savedInstanceState)
            mDialogView.map_view.onResume()
            mDialogView.map_view.getMapAsync(this)
            val mAlertDialog = mBuilder.show()
            mDialogView.ok_btn.setOnClickListener {
                mAlertDialog.dismiss()
            }
        }

        //menu per selezionare foto da camera o da galleria
        registerForContextMenu(change_image)

        //calendario per selezione data
        val builder = MaterialDatePicker.Builder.datePicker()
        val constraintBuilder = CalendarConstraints.Builder()
        val calendar = Calendar.getInstance()
        constraintBuilder.setValidator(ValidDays()) // make available only the days starting from tomorrow
        constraintBuilder.setStart(calendar.timeInMillis) // starts from the current month
        calendar.roll(Calendar.YEAR, 2)
        constraintBuilder.setEnd(calendar.timeInMillis) // ends in 2 years
        builder.setCalendarConstraints(constraintBuilder.build())
        val picker = builder.build()
        // show the date picker
        try {
            date.setOnClickListener {picker.show(requireActivity().supportFragmentManager, picker.toString())}
        }catch(e: java.lang.Exception){
            //non fare nulla, ma almeno non crasha
        }
        // update the text field with the selected date
        picker.addOnPositiveButtonClickListener {
            val simpleFormat = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
            date.setText(simpleFormat.format(it))
            date_wrapper.error = null
        }

        //impostazione spinner categorie
        val catArray = resources.getStringArray(R.array.categories)
        val adapter = CustomArrayAdapter(
            requireContext(),
            R.layout.dropdown_menu_popup_item,
            catArray
        )
        category.setAdapter(adapter)
        category.setOnItemClickListener { parent, _, position, _ ->
            val subadapter = ArrayAdapter.createFromResource(requireContext(), idMap[parent.getItemAtPosition(position)]!!, R.layout.dropdown_menu_popup_item)
            subcategory.setText("")
            subcategory.setAdapter(subadapter)
            subcategory.isEnabled = true
        }
        //impostazione spinner status del prodotto
        val statusArray = resources.getStringArray(R.array.status)
        val statusAdapter = CustomArrayAdapter(
            requireContext(),
            R.layout.dropdown_menu_popup_item,
            statusArray
        )
        status.setAdapter(statusAdapter)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        if(rotation != 0){
            outState.putInt("rotazione", rotation)
        }
        outState.putString("currentPath", currentPhotoPath)
        outState.putString("category", category.text.toString())
        outState.putString("subcategory", subcategory.text.toString())
        outState.putString("status", status.text.toString())
        outState.putBoolean("hasErrors", hasErrors)
    }

    class ValidDays : CalendarConstraints.DateValidator {
        @JvmField val CREATOR: Parcelable.Creator<ValidDays?> = object : Parcelable.Creator<ValidDays?> {
                override fun createFromParcel(source: Parcel): ValidDays {
                    return ValidDays()
                }
                override fun newArray(size: Int): Array<ValidDays?> {
                    return arrayOfNulls(size)
                }
            }
        override fun writeToParcel(dest: Parcel?, flags: Int) {
        }
        override fun isValid(date: Long): Boolean {
            val utc = Calendar.getInstance()
            utc.timeInMillis = date
            val currentTime = Calendar.getInstance()
            return utc > currentTime
        }
        override fun describeContents(): Int {
            return 0
        }

        override fun hashCode(): Int {
            val hashedFields = arrayOf<Any>()
            return hashedFields.contentHashCode()
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as ValidDays

            if (CREATOR != other.CREATOR) return false

            return true
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.item_save_menu, menu)
    }

    private fun changeErrorStatus(){
        /*tolgo stato errore se focus sull'elemento*/
        title.setOnFocusChangeListener { _, hasFocus -> if(hasFocus){
            title_wrapper.error=null
        } }
        price.setOnFocusChangeListener { _, hasFocus -> if(hasFocus){
            price_wrapper.error=null
        } }
        category.setOnFocusChangeListener { _, hasFocus -> if(hasFocus){
            category_wrapper.error=null
        } }
        subcategory.setOnFocusChangeListener { _, hasFocus -> if(hasFocus){
            subcategory_wrapper.error=null
        } }
        location.setOnFocusChangeListener { _, hasFocus -> if(hasFocus){
            location_wrapper.error=null
        } }
    }

    private fun saveData(): Boolean {

        var controlsPassed = true

        /*controllo campi input*/
        if(title.text.isNullOrBlank()){
            controlsPassed=false
            title.clearFocus()
            title_wrapper.error="Title required"
        }
        if(price.text.isNullOrBlank()){
            controlsPassed=false
            price.clearFocus()
            price_wrapper.error="Price required"
        }
        if(category.text.isNullOrBlank()){
            controlsPassed=false
            category.clearFocus()
            category_wrapper.error="Category required"
        }
        if(subcategory.text.isNullOrBlank()){
            controlsPassed=false
            subcategory.clearFocus()
            subcategory_wrapper.error="Subcategory required"
        }
        if(location.text.isNullOrBlank()){
            controlsPassed=false
            location.clearFocus()
            location_wrapper.error="Location required"
        }
        if(date.text.isNullOrBlank()){
            controlsPassed=false
            date.clearFocus()
            date_wrapper.error="Expiry date required"
        }

        if(!controlsPassed) {
            hasErrors=true
            changeErrorStatus()
            Snackbar.make(requireView(), "Some fields contain errors", Snackbar.LENGTH_LONG).setTextColor(Color.rgb(0xFF, 0x66, 0x66)).show()
            return false
        }

        val item = Item(
            id = itemId,
            title = title.text.toString(),
            price = price.text.toString(),
            location = location.text.toString(),
            category = category.text.toString(),
            subcategory = subcategory.text.toString(),
            description = description.text.toString(),
            date = date.text.toString(),
            seller = uvm.me.value?.id!!,
            status = status.text.toString(),
            lat = ivm.latTmp,
            lng = ivm.lngTmp
        )

        if(itemId == ""){
            ivm.addItem(item)
        }else{
            ivm.updateItem(item)
            if(ivm.item.value?.status!! != item.status){
                ivm.getInterestedUserIds(item, item.status)
            }
        }

        ivm.resultId.observe(viewLifecycleOwner, Observer {
            if (lifecycle.currentState != Lifecycle.State.STARTED) {
                if(ivm.resultId.value != null) {
                    if (it != "") {
                        Snackbar.make(requireView(), "Saved!", Snackbar.LENGTH_LONG).show()
                        ivm.itemSelected = it!!
                        ivm.mine = true
                        if (rotation != 0 || ivm.image_tmp != null) {
                            ivm.saveItemImage(it + IMAGE_EXTENSION, ivm.image_tmp!!)
                        }
                        ivm.image = ivm.image_tmp
                        ivm.image_tmp = null
                        ivm.resultId.value = null
                        if (findNavController().currentDestination?.id != R.id.itemDetailsFragment) {
                            findNavController().navigate(R.id.edit_to_list)
                            findNavController().navigate(R.id.list_to_detail)
                        }
                    } else {
                        Snackbar.make(requireView(), "Impossible to contact DataBase", Snackbar.LENGTH_LONG).show()
                    }
                }
            }
        })

        return controlsPassed
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.save_item -> {
                saveData()
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val inflater = requireActivity().menuInflater
        inflater.inflate(R.menu.camera_menu,menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.from_gallery -> {
                ImageManager.takeFromGallery(this)
                true
            }
            R.id.from_camera -> {
                currentPhotoPath = ImageManager.takeFromCamera(requireContext(), this)
                true
            }
            else -> super.onContextItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == requestImageCamera && resultCode == AppCompatActivity.RESULT_OK) {
            rotation = 0
            val file = File(currentPhotoPath!!)
            var imgBitmap = BitmapFactory.decodeFile(file.path)
            imgBitmap = ImageManager.resizeImageIfNeeded(imgBitmap)
            imgBitmap = ImageManager.autoRotateImage(requireContext(), file, imgBitmap)
            product_image.setImageBitmap(imgBitmap)
            ivm.image_tmp = imgBitmap
        }else if (requestCode == requestImageGallery && resultCode == AppCompatActivity.RESULT_OK && data != null){
            rotation = 0
            val imgArray = requireContext().contentResolver.openInputStream(data.data!!)?.readBytes()
            var imgBitmap = BitmapFactory.decodeByteArray(imgArray, 0, imgArray?.size!!)
            imgBitmap = ImageManager.resizeImageIfNeeded(imgBitmap)
            product_image.setImageBitmap(imgBitmap)
            ivm.image_tmp = imgBitmap
        }
    }
}