package it.polito.mad.mad401.lab2.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.ListenerRegistration
import it.polito.mad.mad401.lab2.R
import it.polito.mad.mad401.lab2.data.InterestedUserAdapter
import it.polito.mad.mad401.lab2.data.ItemsViewModel
import it.polito.mad.mad401.lab2.data.UsersViewModel
import kotlinx.android.synthetic.main.users_interested_list_fragment.*

class UsersInterestedFragment : Fragment() {


    private var uvm: UsersViewModel = UsersViewModel()
    private var ivm :ItemsViewModel = ItemsViewModel()
    private var itemId =""
    lateinit var adapter : InterestedUserAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.users_interested_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        uvm = ViewModelProvider(requireActivity()).get(UsersViewModel::class.java)
        ivm = ViewModelProvider(requireActivity()).get(ItemsViewModel::class.java)

        itemId = ivm.itemSelected

        uvm.interestedUsers.observe(viewLifecycleOwner, Observer {
            if (lifecycle.currentState != Lifecycle.State.STARTED) {
                adapter = InterestedUserAdapter(it, itemId, uvm, ivm, viewLifecycleOwner)
                recycler_view.adapter = adapter
                swiperefresh.isRefreshing = false
                if (it.isEmpty()) {
                    no_items.visibility = View.VISIBLE
                } else {
                    no_items.visibility = View.GONE
                }
            }
        })

        uvm.getInterestedUsers(itemId)

        uvm.getUsersImageUrls()

        recycler_view.layoutManager = LinearLayoutManager(requireContext())

        swiperefresh.setOnRefreshListener {
                uvm.getInterestedUsers(itemId)
        }

    }

    override fun onPause() {
        super.onPause()
        uvm.stopRealtimeListener()
    }

}