package it.polito.mad.mad401.lab2.ui

import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.location.Geocoder
import android.os.Bundle
import android.view.*
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import it.polito.mad.mad401.lab2.R
import it.polito.mad.mad401.lab2.data.ImageManager
import it.polito.mad.mad401.lab2.data.User
import it.polito.mad.mad401.lab2.data.UsersViewModel
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import kotlinx.android.synthetic.main.map_dialog.view.*
import java.io.File
import java.util.*


class EditProfileFragment : Fragment(), OnMapReadyCallback {

    private var uvm: UsersViewModel = UsersViewModel()
    private val requestImageCamera = 1
    private val requestImageGallery = 2
    private var currentPhotoPath: String? = null
    private var rotation = 0
    private var hasErrors = false
    private val IMAGE_EXTENSION: String = ".PNG"
    private lateinit var mMap : GoogleMap

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        uvm = ViewModelProvider(requireActivity()).get(UsersViewModel::class.java)
        if (uvm.me.value?.nickname == "") {
            val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    findNavController().navigate(R.id.editprofile_to_logout)
                }
            }
            requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
        }

        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_edit_profile, container, false)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!
        val position : LatLng
        mMap.uiSettings.isCompassEnabled = true
        mMap.uiSettings.isMapToolbarEnabled = false
        mMap.uiSettings.setAllGesturesEnabled(true)
        val marker = mMap.addMarker(MarkerOptions().position(LatLng(0.0, 0.0)).title("You").visible(false))

        if(uvm.me.value?.lat != null && uvm.me.value?.lng != null){
            position = LatLng(uvm.me.value?.lat!!, uvm.me.value?.lng!!)
            marker.position = position
            marker.showInfoWindow()
            marker.isVisible = true
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15F))
        }

        mMap.setOnMapLongClickListener {
            marker.position = it
            marker.isVisible = true
            uvm.me.value?.lat = marker.position.latitude
            uvm.me.value?.lng = marker.position.longitude
            val geocoder = Geocoder(requireContext(), Locale.getDefault())
            try {
                val address =
                    geocoder.getFromLocation(marker.position.latitude, marker.position.longitude, 1)
                if (address.size == 1) {
                    if (address[0].locality != null) {
                        location.setText(address[0].locality.toString())
                    }
                }
            }catch (e :Exception){
                location.setText("")
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(savedInstanceState !=null){
            hasErrors=savedInstanceState.getBoolean("hasErrors")
            currentPhotoPath = savedInstanceState.getString("currentPath")
        }
        if(hasErrors){
            changeErrorStatus()
        }

        full_name.setText(uvm.me.value?.fullname)
        nickname.setText(uvm.me.value?.nickname)
        location.setText(uvm.me.value?.location)
        email_address.setText(uvm.me.value?.email)
        phone_number.setText(uvm.me.value?.phonenumber)

        if(savedInstanceState != null && savedInstanceState.containsKey("rotazione")){
            rotation = savedInstanceState.getInt("rotazione")
            if(rotation != 0){
                var bitmap = photo_profile.drawable.toBitmap()
                bitmap = ImageManager.rotateImage(bitmap, rotation)
                photo_profile.setImageBitmap(bitmap)
            }
        }

        rotateButton.setOnClickListener {
            var bitmap = photo_profile.drawable.toBitmap()
            bitmap = ImageManager.rotateImage(bitmap, 90)
            photo_profile.setImageBitmap(bitmap)
            rotation = (rotation + 90)%360
            uvm.image_tmp = bitmap
        }

        map_edit.setOnClickListener {
            val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.map_dialog, null)
            val mBuilder = AlertDialog.Builder(requireContext()).setView(mDialogView)
            mDialogView.map_view.onCreate(savedInstanceState)
            mDialogView.map_view.onResume()
            mDialogView.map_view.getMapAsync(this)
            val mAlertDialog = mBuilder.show()
            mDialogView.ok_btn.setOnClickListener {
                mAlertDialog.dismiss()
            }
        }

        if(uvm.image_tmp == null) {
            uvm.getMyImageUrl(uvm.currentUser?.uid!! + IMAGE_EXTENSION)
            uvm.myImageUrl.observe(viewLifecycleOwner, Observer {
                if (lifecycle.currentState != Lifecycle.State.STARTED) {
                    if (it != "") {
                        Glide.with(this)
                            .load(it)
                            .placeholder(R.drawable.profile_placeholder)
                            .into(photo_profile)
                    } else {
                        photo_profile.setImageResource(R.drawable.profile_placeholder)
                    }
                }
            })
        }else{
            photo_profile.setImageBitmap(uvm.image_tmp)
        }
        registerForContextMenu(change_image)
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.save_profile_changes, menu)
        return
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.save_profile -> {
                saveProfile()
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        if(rotation != 0){
            outState.putInt("rotazione", rotation)
        }
        outState.putString("currentPath", currentPhotoPath)
        outState.putBoolean("hasErrors", hasErrors)
    }

    private fun String.isEmailInvalid(): Boolean{
        return !android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
    }

    private fun saveProfile(): Boolean{
        var controlsPassed = true
        /*controllo campi input*/

        if(full_name.text.isNullOrBlank()){
            controlsPassed=false
            full_name.clearFocus()
            full_name_wrapper.error="Name required"
        }
        if(nickname.text.isNullOrBlank()){
            controlsPassed=false
            nickname.clearFocus()
            nickname_wrapper.error="Nickname required"
        }
        if(email_address.text.isNullOrBlank() || email_address.text.toString().isEmailInvalid()){
            controlsPassed=false
            email_address.clearFocus()
            email_address_wrapper.error="Valid email required"
        }
        if(location.text.isNullOrBlank()){
            controlsPassed=false
            location.clearFocus()
            location_wrapper.error="Location required"
        }

        if(!controlsPassed) {
            hasErrors=true
            changeErrorStatus()
            Snackbar.make(requireView(), "Some fields contain errors", Snackbar.LENGTH_LONG).setTextColor(Color.rgb(0xFF, 0x66, 0x66)).show()
            return false
        }

        if(rotation != 0 || uvm.image_tmp != null) {
            uvm.saveUserImage(uvm.currentUser?.uid!!+IMAGE_EXTENSION, uvm.image_tmp!!)
        }
        val user = User(
            id = uvm.currentUser!!.uid,
            fullname = full_name.text.toString(),
            nickname = nickname.text.toString(),
            location = location.text.toString(),
            email = email_address.text.toString(),
            phonenumber = phone_number.text.toString(),
            lat = uvm.me.value?.lat,
            lng = uvm.me.value?.lng,
            rating = uvm.me.value?.rating?:0F,
            rating_number = uvm.me.value?.rating_number?:0
        )

        uvm.updateUser(user)

        uvm.result.observe(viewLifecycleOwner, Observer {
            if (lifecycle.currentState != Lifecycle.State.STARTED) {
                if(it != null) {
                    uvm.result.value = null
                    if (it) {
                        Snackbar.make(requireView(), "Saved!", Snackbar.LENGTH_SHORT).show()
                        uvm.image = uvm.image_tmp
                        uvm.image_tmp = null
                        if (findNavController().currentDestination?.id != R.id.nav_showprofile) {
                            findNavController().navigate(R.id.editprofile_to_onsalelist)
                            findNavController().navigate(R.id.onsalelist_to_showprofile)
                        }
                    } else {
                        Snackbar.make(requireView(), "Impossible to contact DB", Snackbar.LENGTH_SHORT).show()
                    }
                }
            }
        })

        return controlsPassed
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val inflater = requireActivity().menuInflater
        inflater.inflate(R.menu.camera_menu,menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.from_gallery -> {
                ImageManager.takeFromGallery(this)
                true
            }
            R.id.from_camera -> {
                currentPhotoPath = ImageManager.takeFromCamera(requireContext(), this)
                true
            }
            else -> super.onContextItemSelected(item)
        }
    }

    private fun changeErrorStatus(){
        /*tolgo stato errore se focus sull'elemento*/
        full_name.setOnFocusChangeListener { _, hasFocus -> if(hasFocus){
            full_name_wrapper.error=null
        } }
        nickname.setOnFocusChangeListener { _, hasFocus -> if(hasFocus){
            nickname_wrapper.error=null
        } }
        email_address.setOnFocusChangeListener { _, hasFocus -> if(hasFocus){
            email_address_wrapper.error=null
        } }
        location.setOnFocusChangeListener { _, hasFocus -> if(hasFocus){
            location_wrapper.error=null
        } }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == requestImageCamera && resultCode == AppCompatActivity.RESULT_OK) {
            rotation = 0
            val file = File(currentPhotoPath!!)
            var imgBitmap = BitmapFactory.decodeFile(file.path)
            imgBitmap = ImageManager.resizeImageIfNeeded(imgBitmap)
            imgBitmap = ImageManager.autoRotateImage(requireContext(), file, imgBitmap)
            uvm.image_tmp = imgBitmap
            photo_profile.setImageBitmap(imgBitmap)
        }else if (requestCode == requestImageGallery && resultCode == AppCompatActivity.RESULT_OK && data != null){
            rotation = 0
            val imgArray = requireActivity().contentResolver?.openInputStream(data.data!!)?.readBytes()
            var imgBitmap = BitmapFactory.decodeByteArray(imgArray, 0, imgArray?.size!!)
            imgBitmap = ImageManager.resizeImageIfNeeded(imgBitmap)
            uvm.image_tmp = imgBitmap
            photo_profile.setImageBitmap(imgBitmap)
        }
    }
}