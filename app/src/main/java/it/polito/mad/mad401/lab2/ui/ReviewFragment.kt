package it.polito.mad.mad401.lab2.ui

import android.graphics.Color
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import it.polito.mad.mad401.lab2.R
import it.polito.mad.mad401.lab2.data.FocusedItem
import it.polito.mad.mad401.lab2.data.ItemsViewModel
import it.polito.mad.mad401.lab2.data.UsersViewModel
import kotlinx.android.synthetic.main.rating_fragment.*

class ReviewFragment: Fragment() {

    private lateinit var uvm: UsersViewModel
    private lateinit var ivm: ItemsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return  inflater.inflate(R.layout.rating_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        uvm = ViewModelProvider(requireActivity()).get(UsersViewModel::class.java)
        ivm = ViewModelProvider(requireActivity()).get(ItemsViewModel::class.java)

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        menu.clear()
        inflater.inflate(R.menu.review_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.button_send -> {
                if (bar_rating.rating.toInt() > 0) {
                    val fi = FocusedItem(
                        user = ivm.review.user,
                        item = ivm.review.item,
                        rating = bar_rating.rating.toInt(),
                        comment = text_review.text.toString(),
                        seller = ivm.review.seller
                    )
                    uvm.addReview(fi)
                    uvm.result.observe(viewLifecycleOwner, Observer {
                        if (lifecycle.currentState != Lifecycle.State.STARTED) {
                            if (it != null) {
                                uvm.result.value = null
                                if (it) {
                                    Snackbar.make(requireView(), "Review published!", 5000)
                                        .setTextColor(Color.WHITE).show()
                                    findNavController().navigateUp()
                                } else {
                                    Snackbar.make(requireView(), "Error", Snackbar.LENGTH_LONG)
                                        .setTextColor(Color.rgb(0xFF, 0x66, 0x66)).show()
                                }
                            }
                        }
                    })
                } else {
                    Snackbar.make(requireView(), "Rate is required!", Snackbar.LENGTH_LONG)
                        .setTextColor(Color.rgb(0xFF, 0x66, 0x66)).show()
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}