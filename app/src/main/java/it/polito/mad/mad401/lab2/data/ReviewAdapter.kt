package it.polito.mad.mad401.lab2.data

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import it.polito.mad.mad401.lab2.R

class ReviewAdapter(
    private val reviews: List<FocusedItem>
): RecyclerView.Adapter<ReviewAdapter.ViewHolder>() {

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val rating: RatingBar =  v.findViewById(R.id.single_review_rating)
        val text: TextView = v.findViewById(R.id.single_review_text)
        fun bind(fi: FocusedItem){
            rating.rating = fi.rating.toFloat()
            text.text = fi.comment
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.single_review, parent, false) //false per non connetterlo subito

        return ViewHolder(v)
    }

    override fun getItemCount() = reviews.size

    override fun onBindViewHolder(holder: ReviewAdapter.ViewHolder, position: Int) {
        holder.bind(reviews[position])
    }
}