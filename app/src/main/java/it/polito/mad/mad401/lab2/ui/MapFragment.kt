package it.polito.mad.mad401.lab2.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import it.polito.mad.mad401.lab2.R
import it.polito.mad.mad401.lab2.data.ItemsViewModel
import it.polito.mad.mad401.lab2.data.UsersViewModel
import kotlinx.android.synthetic.main.map_fragment.*

class MapFragment : Fragment(), OnMapReadyCallback {

    private lateinit var mMap : GoogleMap
    private var ivm: ItemsViewModel = ItemsViewModel()
    private var uvm: UsersViewModel = UsersViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.map_fragment, container, false)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!
        val positions = mutableListOf<LatLng>()
        if(ivm.item.value?.lat != null && ivm.item.value?.lng != null){
            positions.add(LatLng(ivm.item.value?.lat!!, ivm.item.value?.lng!!))
            mMap.addMarker(MarkerOptions().position(positions[0]).title("Item"))
            if(uvm.me.value?.lat != null && uvm.me.value?.lng != null){
                positions.add(LatLng(uvm.me.value?.lat!!, uvm.me.value?.lng!!))
                mMap.addMarker(MarkerOptions().position(positions[1]).title("You"))
                mMap.addPolyline(PolylineOptions()).points = positions
            }
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(positions[0], 12F))
        }
        mMap.uiSettings.isCompassEnabled = true
        mMap.uiSettings.isMapToolbarEnabled = false
        mMap.uiSettings.isZoomControlsEnabled = true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivm = ViewModelProvider(requireActivity()).get(ItemsViewModel::class.java)
        uvm = ViewModelProvider(requireActivity()).get(UsersViewModel::class.java)

        mapview.onCreate(savedInstanceState)
        mapview.onResume()
        mapview.getMapAsync(this)
    }

}