package it.polito.mad.mad401.lab2.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import it.polito.mad.mad401.lab2.R
import it.polito.mad.mad401.lab2.data.UsersViewModel
import kotlinx.coroutines.*


class LogoutFragment : Fragment() {

    private var uvm: UsersViewModel = UsersViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        uvm = ViewModelProvider(requireActivity()).get(UsersViewModel::class.java)
        signOut()
    }

    private fun signOut() {
        FirebaseAuth.getInstance().signOut()
        FirebaseMessaging.getInstance().isAutoInitEnabled = false
        MainScope().launch { withContext(Dispatchers.IO){FirebaseInstanceId.getInstance().deleteInstanceId()} }
        uvm.currentUser = null
        val mGoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        GoogleSignIn.getClient(requireContext(), mGoogleSignInOptions).signOut()
        if(findNavController().currentDestination?.id != R.id.nav_onsalelist_items) {
            findNavController().navigate(R.id.logout_to_onsale)
        }
    }
}
