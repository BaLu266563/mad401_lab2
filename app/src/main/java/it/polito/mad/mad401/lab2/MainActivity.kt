package it.polito.mad.mad401.lab2

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.bumptech.glide.Glide
import com.google.android.material.navigation.NavigationView
import it.polito.mad.mad401.lab2.data.UsersViewModel


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val uvm :UsersViewModel = ViewModelProvider(this).get(UsersViewModel::class.java)

        setContentView(R.layout.activity_main)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(setOf(R.id.nav_list_items, R.id.nav_onsalelist_items, R.id.nav_items_of_interest, R.id.nav_bought_items, R.id.nav_logout), drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        val navHeader = navView.getHeaderView(0)
        uvm.me.observe(this, Observer {
            navHeader.findViewById<TextView>(R.id.nickname_header).text = it.fullname
            navHeader.findViewById<TextView>(R.id.email_header).text = it.email
        })
        uvm.myImageUrl.observe(this, Observer {
            if(it != "") {
                Glide.with(this)
                    .load(it)
                    .placeholder(R.drawable.product_placeholder)
                    .into(navHeader.findViewById<ImageView>(R.id.profile_image_header))
            }
            else{
                navHeader.findViewById<ImageView>(R.id.profile_image_header).setImageResource(R.drawable.profile_placeholder)
            }
        })

    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}
