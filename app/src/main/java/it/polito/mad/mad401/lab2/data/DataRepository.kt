package it.polito.mad.mad401.lab2.data

import android.graphics.Bitmap
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.*
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ListResult
import com.google.firebase.storage.ktx.storage
import java.io.ByteArrayOutputStream

class DataRepository {

    var item = MutableLiveData<Item>()
    var items = MutableLiveData<MutableList<Item>>()
    var user = MutableLiveData<User>()

    private val db :FirebaseFirestore by lazy{
        FirebaseFirestore.getInstance()
    }
    private val st :FirebaseStorage by lazy{
        Firebase.storage
    }

    // USERS
    fun getUser(userId : String) :Task<DocumentSnapshot>{
         return db.collection("users").document(userId).get()
    }

    fun getMe(userId : String) :Task<DocumentSnapshot>{
        return db.collection("users").document(userId).get()
    }

    fun getInterestedUserIds(itemId: String): Task<QuerySnapshot> {
        return db.collection("interests").whereEqualTo("item", itemId).get()
    }

    fun updateUser(user:User) :Task<Void>{
        return db.collection("users").document(user.id).set(user)
    }

    fun addInterestedUser(focusedItem: FocusedItem) :Task<DocumentReference>{
        return db.collection("interests").add(focusedItem)
    }

    fun removeInterestedUser(focusedItem: FocusedItem) :Task<QuerySnapshot>{
        return db.collection("interests").whereEqualTo("user", focusedItem.user).whereEqualTo("item", focusedItem.item).get()
    }

    fun checkInterested(focusedItem: FocusedItem) :Task<QuerySnapshot>{
        return db.collection("interests").whereEqualTo("user", focusedItem.user).whereEqualTo("item", focusedItem.item).get()
    }

    fun realtimeInterested(itemId: String): Query {
        return db.collection("interests").whereEqualTo("item", itemId)
    }

    fun getReviews(userId: String): Task<QuerySnapshot> {
        return db.collection("reviews").whereEqualTo("seller", userId).get()
    }

    fun addReview(focusedItem :FocusedItem): Task<Transaction> {
        val review = db.collection("reviews").document()
        val seller = db.collection("users").document(focusedItem.seller)

        return db.runTransaction{transaction ->
            val snapshot = transaction.get(seller)
            val obj = snapshot.toObject(User::class.java)
            val rating = ((obj!!.rating * obj.rating_number) + focusedItem.rating)/(obj.rating_number+1 )

            transaction.update(seller, mutableMapOf<String, Any>("rating" to rating, "rating_number" to (obj.rating_number+1)))
            transaction.set(review, focusedItem)
        }
    }

    fun checkReview(focusedItem :FocusedItem): Task<QuerySnapshot> {
        return db.collection("reviews").whereEqualTo("user", focusedItem.user).get()
    }

    // ITEMS

    fun getItem(itemId :String): Task<DocumentSnapshot> {
        return db.collection("items").document(itemId).get()
    }

    fun sellItem(focusedItem: FocusedItem): Task<DocumentSnapshot> {
        /*aggiunta a collezione bought*/
        db.collection("boughts").document().set(focusedItem)
        /*rimozione da collezione interests*/
        db.collection("interests").whereEqualTo("user", focusedItem.user).whereEqualTo("item", focusedItem.item).get().addOnSuccessListener {
            result ->
            result.documents.forEach { it.reference.delete() }
        }
        /*settaggio status a sold*/
        return db.collection("items").document(focusedItem.item).get()
    }

    fun getBoughtItems(userId: String): Task<QuerySnapshot> {
        return db.collection("boughts").whereEqualTo("user", userId).get()
    }

    fun getAllItems(){
        db.collection("items").get().addOnSuccessListener { result ->
                val list= mutableListOf<Item>()
                for(doc in result!!){
                    val obj = doc.toObject(Item::class.java)
                    obj.id=doc.id
                    list.add(obj)
                }
                items.value=list
        }
    }

    fun getInterestsForUser(userId: String): Task<QuerySnapshot> {
        return db.collection("interests").whereEqualTo("user", userId).get()
    }

    fun getFilteredItems(userId: String, filterOptions: FilterOptions): Task<MutableList<QuerySnapshot>> {
        var query: Query = db.collection("items").whereEqualTo("status", "Available")

        if(filterOptions.byCategory){
            query = query.whereEqualTo("category", filterOptions.filter_category)
        }
        if(filterOptions.bySubcategory) {
            query = query.whereEqualTo("subcategory", filterOptions.filter_subcategory)
        }
        val task1 = query.whereGreaterThan("seller", userId).get()
        val task2 = query.whereLessThan("seller", userId).get()

        return Tasks.whenAllSuccess<QuerySnapshot>(task1, task2)
    }

    fun getMyItems(userId :String): Task<QuerySnapshot> {
        return db.collection("items").whereEqualTo("seller", userId).get()
    }

    fun getRealTimeItem(itemId:String): DocumentReference {
        return db.collection("items").document(itemId)
    }

    fun addItem(item : Item): Task<DocumentReference> {
        return db.collection("items").add(item)
    }

    fun updateItem(item:Item): Task<Void> {
        return db.collection("items").document(item.id).set(item)
    }

    // TOKEN
    fun getToken(userId :String) : Task<DocumentSnapshot> {
        return db.collection("tokens").document(userId).get()
    }


    // IMAGES
    fun saveItemImage(name: String, image: Bitmap){
        var stream = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.PNG, 100, stream)
        val byteArray: ByteArray = stream.toByteArray()
        Firebase.storage.reference.child("items/$name").putBytes(byteArray)
    }

    fun getItemImageUrl(imageName: String): Task<Uri> {
        return Firebase.storage.reference.child("items/$imageName").downloadUrl
    }

    fun getUserImageUrl(imageName: String): Task<Uri> {
        return Firebase.storage.reference.child("users/$imageName").downloadUrl
    }
    fun getMyImageUrl(imageName: String): Task<Uri> {
        return Firebase.storage.reference.child("users/$imageName").downloadUrl
    }

    fun saveUserImage(name: String, image: Bitmap){
        var stream = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.PNG, 100, stream)
        val byteArray: ByteArray = stream.toByteArray()
        Firebase.storage.reference.child("users/$name").putBytes(byteArray)
    }

    fun getItemsImageUrls(): Task<ListResult> {
        return Firebase.storage.reference.child("items/").listAll()
    }

    fun getUsersImageUrls(): Task<ListResult> {
        return Firebase.storage.reference.child("users/").listAll()
    }
}