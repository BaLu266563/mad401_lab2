package it.polito.mad.mad401.lab2.data

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.ListenerRegistration
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.List
import kotlin.collections.Map
import kotlin.collections.MutableList
import kotlin.collections.MutableMap
import kotlin.collections.forEach
import kotlin.collections.mutableListOf
import kotlin.collections.mutableMapOf
import kotlin.collections.set


class FilterOptions(){
    var byText: Boolean = false
    var byCategory: Boolean = false
    var bySubcategory: Boolean = false
    var byPriceMin: Boolean = false
    var byPriceMax: Boolean = false

    var filter_text = ""
    var filter_category = ""
    var filter_subcategory = ""
    var filter_prize_min = ""
    var filter_prize_max = ""
}


class ItemsViewModel : ViewModel() {

    // variabili generiche
    lateinit var review : FocusedItem
    val myRepository = DataRepository()
    val filterOptions = FilterOptions()
    private lateinit var context : Context
    private val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(context)
    }
    private val serverKey =
        "key=" + "AAAAEEXo7GE:APA91bE0-JJ9ycYCRk8Brqth1gHo2RwF7vtI5Aq0mRcdydQj5rZbF0t2rD1uD4Yq8-c_-ZipoypvP5W-EeVSQEqVqH5B0mvEjQWVhW64hNlVyVfBDW-t2nxTZGR-l-6TRUSEx4-zu8Xb"
    private val FCM_API = "https://fcm.googleapis.com/fcm/send"
    var realTimeListener: ListenerRegistration? = null

    // variabili da usare per passare dati tra fragment
    var itemSelected :String = ""
    var mine :Boolean = false

    // variabili per valori temporanei
    var latTmp :Double? = null
    var lngTmp :Double? = null
    var image: Bitmap? = null
    var image_tmp: Bitmap? = null

    // variabili per gestire DB
    var result = MutableLiveData<Boolean>()
    var boughtItems = MutableLiveData<MutableList<Item>>()
    var interestedItems= MutableLiveData<MutableList<Item>>()
    var items = MutableLiveData<MutableList<Item>>()
    var myItems = MutableLiveData<MutableList<Item>>()
    var item = MutableLiveData<Item>()
    var resultId = MutableLiveData<String?>()
    var itemImageUrl = MutableLiveData<String>()
    var itemsImageUrls = MutableLiveData<MutableMap<String, String>>()

    fun setContext(ctx :Context){
        context = ctx
    }

    fun getItemImageUrl(url: String){
        myRepository.getItemImageUrl(url)
            .addOnSuccessListener {
                itemImageUrl.value = it.toString()
            }.addOnFailureListener {
                itemImageUrl.value = ""
            }
    }

    fun getInterestForUser(userId: String){
        myRepository.getInterestsForUser(userId)
            .addOnSuccessListener { result ->
                val list = mutableListOf<Task<DocumentSnapshot>>()
                for(doc in result.documents){
                    val obj = doc.toObject(FocusedItem::class.java)
                    list.add(myRepository.getItem(obj?.item!!))
                }
                Tasks.whenAllSuccess<DocumentSnapshot>(list).addOnSuccessListener { docList ->
                    val items = mutableListOf<Item>()
                    for(doc in docList){
                        val obj =doc.toObject(Item::class.java)
                        items.add(obj!!)
                    }
                    interestedItems.value = items
                }
            }
    }

    fun getItemsImageUrls(){
        myRepository.getItemsImageUrls()
            .addOnSuccessListener { results ->
                val map = mutableMapOf<String, String>()
                results.items.forEach{res->
                    res.downloadUrl.addOnSuccessListener {
                        map[res.name.removeSuffix(".PNG")] = it.toString()
                        itemsImageUrls.value = map
                    }
                }
            }
    }

    fun sellItem(focusedItem: FocusedItem){
        myRepository.sellItem(focusedItem)
            .addOnSuccessListener { res ->
                var obj = res.toObject(Item::class.java)
                obj?.id = res.id
                obj?.status="Sold"
                updateItem(obj!!)
            }
    }

    fun getBoughtItems(userId: String){
        myRepository.getBoughtItems(userId)
            .addOnSuccessListener { result ->
                val list = mutableListOf<Task<DocumentSnapshot>>()
                for(doc in result.documents){
                    val obj = doc.toObject(FocusedItem::class.java)
                    list.add(myRepository.getItem(obj?.item!!))
                }
                Tasks.whenAllSuccess<DocumentSnapshot>(list).addOnSuccessListener { docList ->
                    val items = mutableListOf<Item>()
                    for(doc in docList){
                        val obj =doc.toObject(Item::class.java)
                        items.add(obj!!)
                    }
                    boughtItems.value = items
                }
            }
    }

    fun getInterestedUserIds(item: Item, status: String){
        myRepository.getInterestedUserIds(item.id)
            .addOnSuccessListener { result ->
                val list= mutableListOf<String>()
                for (doc in result){
                    list.add(doc.get("user").toString())
                }
                sendNotificationsToRemote(list, item.title, "The item is now $status")
                Log.d("query", "GetInterestedUsers successful")
            }
            .addOnFailureListener {
                Log.d("query", "GetInterestedUsers failed")
            }
    }

    fun getFilteredItems(userId: String, filterOptions: FilterOptions){
        myRepository.getFilteredItems(userId, filterOptions)
            .addOnSuccessListener { querySnapshots ->
                val list= mutableListOf<Item>()
                for(querySnapshot in querySnapshots){
                    for(doc in querySnapshot){
                        val obj = doc.toObject(Item::class.java)
                        obj.id=doc.id

                        var price: Float = obj.price.toFloat()
                        var minPrice: Float = 0F
                        var maxPrice: Float = 0F
                        if(filterOptions.filter_prize_min != "" && filterOptions.filter_prize_max != ""){
                            minPrice = filterOptions.filter_prize_min.toFloat()
                            maxPrice = filterOptions.filter_prize_max.toFloat()
                        }
                        /*check on date, if expired or not*/
                        val formatter = SimpleDateFormat("dd/MM/yyyy", Locale.ITALY)
                        val date = formatter.parse(obj.date)!!
                        val today = Calendar.getInstance().time

                        if(today.before(date)) {
                            if (filterOptions.byText) {
                                if (obj.title.contains(filterOptions.filter_text, true)) {
                                    if (filterOptions.byPriceMin) {
                                        if (filterOptions.byPriceMax) {
                                            if ((price <= maxPrice) and (price >= minPrice)) list.add(
                                                obj
                                            )
                                        } else {
                                            if (price >= minPrice) list.add(obj)
                                        }
                                    } else {
                                        if (filterOptions.byPriceMax) {
                                            if (price <= maxPrice) list.add(obj)
                                        } else {
                                            list.add(obj)
                                        }
                                    }
                                }
                            } else {
                                if (filterOptions.byPriceMin) {
                                    if (filterOptions.byPriceMax) {
                                        if ((price <= maxPrice) and (price >= minPrice)) list.add(
                                            obj
                                        )
                                    } else {
                                        if (price >= minPrice) list.add(obj)
                                    }
                                } else {
                                    if (filterOptions.byPriceMax) {
                                        if (price <= maxPrice) list.add(obj)
                                    } else {
                                        list.add(obj)
                                    }
                                }
                            }
                        }
                    }
                }
                items.value=list
            }
    }

    fun getMyItems(userId:String){
        myRepository.getMyItems(userId)
            .addOnSuccessListener { result ->
                val list = mutableListOf<Item>()
                for (doc in result!!) {
                    val obj = doc.toObject(Item::class.java)
                    obj.id = doc.id
                    list.add(obj)
                }
                myItems.value = list
            }
            .addOnFailureListener {
                Log.d("query", "getMyItems failed")
            }
    }

    fun addItem(item: Item){
        myRepository.addItem(item)
            .addOnSuccessListener {
                resultId.value = it.id
                item.id = it.id
                this.item.value = item
            }.addOnFailureListener {
                resultId.value = ""
            }
    }

    fun updateItem(item: Item){
        myRepository.updateItem(item)
            .addOnSuccessListener {
                resultId.value = item.id
                this.item.value = item
            }.addOnFailureListener {
                resultId.value = ""
            }
    }

    fun addInterest(focusedItem: FocusedItem){
        myRepository.addInterestedUser(focusedItem)
            .addOnSuccessListener {
                result.value = true
            }
            .addOnFailureListener {
                result.value = false
            }
    }

    fun removeInterestedUser(focusedItem: FocusedItem){
        myRepository.removeInterestedUser(focusedItem)
            .addOnSuccessListener{
                if(it.documents.size != 0) {
                    it.documents[0].reference.delete()
                        .addOnSuccessListener {
                            result.value = true
                        }
                        .addOnFailureListener {
                            result.value = false
                        }
                }
            }
            .addOnFailureListener {
                result.value = false
            }
    }

    fun saveItemImage(name: String, image: Bitmap){
        myRepository.saveItemImage(name, image)
    }

    fun getRealTimeItem(itemId:String){
        realTimeListener = myRepository.getRealTimeItem(itemId)
            .addSnapshotListener{ snap, e ->
                if(e!=null){
                    return@addSnapshotListener
                }
                if(snap != null){
                    snap.reference.get().addOnSuccessListener {
                        val obj = it.toObject(Item::class.java)
                        obj!!.id = it.id
                        item.value=obj
                    }
                }
            }
    }

    fun stopRealTimeItem(){
        realTimeListener?.remove()
    }

    fun sendNotificationsToRemote(list: List<String>, title: String,message : String){
        for(userId in list){
            sendNotificationToRemote(userId, title, message)
        }
    }

    fun sendNotificationToRemote(sellerId :String, title :String, message :String){
        myRepository.getToken(sellerId).addOnSuccessListener {
            val notification = JSONObject()
            val notificationBody = JSONObject()

            try {
                notificationBody.put("title", title)
                notificationBody.put("body", message)

                notification.put("to", it.get("token"))
                notification.put("notification", notificationBody)

            } catch (e: JSONException) {
            }
            sendNotification(notification)
        }
    }

    private fun sendNotification(notification: JSONObject) {
        val jsonObjectRequest = object : JsonObjectRequest(FCM_API, notification,
            Response.Listener<JSONObject> { response ->
                Log.i("TAG", "onResponse: $response")
            },
            Response.ErrorListener {
                Log.i("TAG", "onErrorResponse: Didn't work")
            }) {

            override fun getHeaders(): Map<String, String> {
                val params = HashMap<String, String>()
                params["Authorization"] = serverKey
                params["Content-Type"] = "application/json"
                return params
            }
        }
        requestQueue.add(jsonObjectRequest)
    }
}