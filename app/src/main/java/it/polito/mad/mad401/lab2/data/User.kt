package it.polito.mad.mad401.lab2.data


data class User (
    var fullname : String="",
    var nickname : String="",
    var email : String="",
    var phonenumber : String="",
    var location : String="",
    var id : String="",
    var lat : Double?=null,
    var lng : Double?=null,
    var rating : Float = 0F,
    var rating_number : Int = 0
)