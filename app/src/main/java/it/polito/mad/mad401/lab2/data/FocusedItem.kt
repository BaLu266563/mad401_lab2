package it.polito.mad.mad401.lab2.data

class FocusedItem(var item:String = "",
                  var user:String="",
                  var rating : Int = -1,
                  var comment : String = "",
                  var seller: String = "")