package it.polito.mad.mad401.lab2.data

import android.graphics.Bitmap
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.ListenerRegistration

class UsersViewModel : ViewModel() {

    // variabili generiche
    val myRepository = DataRepository()
    var currentUser :FirebaseUser? = null
    var realTimeListener: ListenerRegistration? = null

    // variabili da usare per passare informazioni tra fragment
    var image: Bitmap? = null
    var image_tmp :Bitmap? = null

    // variabili per gestire informazioni ricevute dal DB
    var me = MutableLiveData<User>()
    var user = MutableLiveData<User>()
    var userImageUrl = MutableLiveData<String>()
    var myImageUrl = MutableLiveData<String>()
    var usersImageUrls = MutableLiveData<MutableMap<String, String>>()
    var focusedItems = MutableLiveData<MutableMap<String, FocusedItem>>()
    var reviews = MutableLiveData<MutableList<FocusedItem>>()
    var interestedUsers = MutableLiveData<MutableList<User>>()
    var result = MutableLiveData<Boolean?>()

    fun getReviews(userId: String){
        myRepository.getReviews(userId)
            .addOnSuccessListener {
                    querySnapshot ->
                val list = mutableListOf<FocusedItem>()
                querySnapshot.documents.forEach {
                    val obj = it.toObject(FocusedItem::class.java)
                    list.add(obj!!)
                }
                reviews.value = list
            }
    }

    fun checkReview(focusedItem :FocusedItem){
        myRepository.checkReview(focusedItem)
            .addOnSuccessListener {
                val map = mutableMapOf<String, FocusedItem>()
                it.documents.forEach { doc ->
                    map[doc.get("item").toString()] = doc.toObject(FocusedItem::class.java)!!
                }
                focusedItems.value = map
            }
            .addOnFailureListener {
                Log.d("review", "Review check failed")
            }
    }

    fun getUsersImageUrls(){
        myRepository.getUsersImageUrls()
            .addOnSuccessListener { results ->
                val map = mutableMapOf<String, String>()
                results.items.forEach{res->
                    res.downloadUrl.addOnSuccessListener {
                        map[res.name.removeSuffix(".PNG")] = it.toString()
                        usersImageUrls.value = map
                    }
                }
            }
    }

    fun getUser (userId : String){
        myRepository.getUser(userId)
            .addOnSuccessListener { doc ->
                Log.d("query", "GetUser successful")
                if(doc.exists()){
                    user.value = doc.toObject(User::class.java)
                }else {
                    user.value = User()
                }
            }
            .addOnFailureListener {
                Log.d("query", "GetUser failed")
            }
    }

    fun getMe(userId : String){
        myRepository.getMe(userId)
            .addOnSuccessListener { doc ->
                Log.d("query", "GetMe successful")
                if(doc.exists()){
                    me.value = doc.toObject(User::class.java)
                }else {
                    me.value = User()
                }
            }
            .addOnFailureListener {
                Log.d("query", "GetMe failed")
            }
    }

    fun updateUser (user :User){
        myRepository.updateUser(user)
            .addOnSuccessListener {
                result.value = true
            }
            .addOnFailureListener {
                result.value = false
            }
    }

    fun checkInterested (focusedItem: FocusedItem){
        myRepository.checkInterested(focusedItem)
            .addOnSuccessListener {
                result.value = !it.isEmpty
            }
            .addOnFailureListener {
                Log.d("query", "error in check interested")
            }
    }

    fun getInterestedUsers(itemId: String){
        myRepository.realtimeInterested(itemId).addSnapshotListener{ snapshot, e ->
            if(e!=null){
                return@addSnapshotListener
            }
            val l = mutableListOf<User>()
            if(snapshot!=null){
                snapshot.documents.forEach{ fis ->
                    val obj = fis.toObject(FocusedItem::class.java)
                    /*db.collection("users").document(obj?.user!!).get()*/
                    myRepository.getUser(obj?.user!!).addOnSuccessListener { usr ->
                        val u = usr.toObject(User::class.java)
                        l.add(u!!)
                        interestedUsers.value = l
                    }
                }
                if(snapshot.documents.size==0){
                    interestedUsers.value = l
                }
            }
        }
    }

    fun stopRealtimeListener(){
        realTimeListener?.remove()
    }


    fun saveUserImage(name: String, image: Bitmap){
        myRepository.saveUserImage(name, image)
    }

    fun getUserImageUrl(url: String){
        myRepository.getUserImageUrl(url)
            .addOnSuccessListener {
                userImageUrl.value =it.toString()
            }.addOnFailureListener {
                userImageUrl.value = ""
            }
    }

    fun getMyImageUrl(url: String){
        myRepository.getMyImageUrl(url)
            .addOnSuccessListener {
                myImageUrl.value =it.toString()
            }.addOnFailureListener {
                myImageUrl.value = ""
            }
    }

    fun addReview(focusedItem: FocusedItem){
        myRepository.addReview(focusedItem)
            .addOnSuccessListener {
                Log.d("review", "success")
                val map = focusedItems.value
                if (map != null) {
                    map[focusedItem.item] = focusedItem
                }
                focusedItems.value = map
                result.value = true
            }.addOnFailureListener{
                result.value = false
                Log.d("review", "error")
            }
    }
}