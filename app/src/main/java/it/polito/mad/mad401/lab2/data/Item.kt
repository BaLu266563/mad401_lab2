package it.polito.mad.mad401.lab2.data

data class Item(var id:String="",
                var title :String="",
                var price :String="",
                var location :String="",
                var category :String="",
                var subcategory :String="",
                var description :String="",
                var date :String="",
                var seller :String="",
                var status :String="Available",
                var lat : Double?=null,
                var lng : Double?=null
                )
