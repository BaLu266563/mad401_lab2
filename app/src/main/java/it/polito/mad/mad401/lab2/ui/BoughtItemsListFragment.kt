package it.polito.mad.mad401.lab2.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import it.polito.mad.mad401.lab2.R
import it.polito.mad.mad401.lab2.data.ItemsViewModel
import it.polito.mad.mad401.lab2.data.SoldItemAdapter
import it.polito.mad.mad401.lab2.data.UsersViewModel
import kotlinx.android.synthetic.main.bought_items_fragment.*


class BoughtItemsListFragment: Fragment(){

    private var ivm : ItemsViewModel = ItemsViewModel()
    private var uvm : UsersViewModel = UsersViewModel()
    lateinit var adapter : SoldItemAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bought_items_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivm = ViewModelProvider(requireActivity()).get(ItemsViewModel::class.java)
        uvm = ViewModelProvider(requireActivity()).get(UsersViewModel::class.java)
        ivm.setContext(requireActivity().applicationContext)

        ivm.getBoughtItems(uvm.currentUser?.uid!!)
        ivm.boughtItems.observe(viewLifecycleOwner, Observer {
            if(lifecycle.currentState != Lifecycle.State.STARTED) {
                adapter = SoldItemAdapter(it, ivm, uvm, viewLifecycleOwner)
                recycler_view.adapter = adapter
                swiperefresh.isRefreshing = false
                if (it.isEmpty()) {
                    no_boughts.visibility = View.VISIBLE
                } else {
                    no_boughts.visibility = View.GONE
                }
            }
        })

        ivm.getItemsImageUrls()
        recycler_view.layoutManager = LinearLayoutManager(context)
        swiperefresh.setOnRefreshListener {
            ivm.getBoughtItems(uvm.currentUser?.uid!!)
            ivm.getItemsImageUrls()
        }

    }
}